﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkSystemCollections
{
    public class ListNumberService
    {
        public static List<int> FillingRandomValue(List<int> listNumbers, int count)
        {
            Random random = new Random();
            for (int i = 0; i < count; i++)
            {
                listNumbers.Add(random.Next(0, 1000));
            }
            return listNumbers;
        }
        public static void FindMaxNumber(List<int> listNumbers)
        {
            int firstMaxNumber = 0;
            int secondMaxNumber = 0;
            foreach (int number in listNumbers)
            {
                if (number > firstMaxNumber)
                {
                    secondMaxNumber = firstMaxNumber;
                    firstMaxNumber = number;
                }
                else if (number > secondMaxNumber)
                {
                    secondMaxNumber = number;
                }
            }
            Console.WriteLine("Максимальное число: ");
            for (int i = 0; i < listNumbers.Count; i++)
            {
                if (listNumbers[i] == firstMaxNumber)
                {
                    Console.WriteLine($"Позиция {i} значение {listNumbers[i]}");
                }
            }
            Console.WriteLine("\nВторое максимальное число: ");
            for (int i = 0; i < listNumbers.Count; i++)
            {
                if (listNumbers[i] == secondMaxNumber)
                {
                    Console.WriteLine($"Позиция {i} значение {listNumbers[i]}");
                }
            }
        }
        public static void SumEvenPosition(List<int> listNumbers)
        {
            int sumElement = 0;
            for (int i = 0; i < listNumbers.Count; i++)
            {
                if (i % 2 == 0)
                {
                    sumElement += listNumbers[i];
                }
            }
            Console.WriteLine($"\nСумма элементов на четных позиция: {sumElement}");
        }
    }
}
