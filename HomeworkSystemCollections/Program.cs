﻿using System;
using System.Collections.Generic;

namespace HomeworkSystemCollections
{
    class Program
    {
        static void Main(string[] args)
        {
            // Задание 1.
            List<int> listNumbers = new List<int>();
            ListNumberService.FillingRandomValue(listNumbers, 30);
            ListNumberService.FindMaxNumber(listNumbers);
            ListNumberService.SumEvenPosition(listNumbers);

            // Задание 2.
            try
            {
                MyCollention myCollention = new MyCollention();
                myCollention.Add(5);
                myCollention.Add(3);
                myCollention.Add(77);
                myCollention.Add(4);
                Console.Write("\nМассив до сортировки: ");
                for (int i = 0; i < myCollention.Count; i++)
                {
                    Console.Write($"{myCollention[i]} ");
                }
                myCollention.Sort(myCollention);
                Console.Write("\nМассив после сортировки: ");
                for (int i = 0; i < myCollention.Count; i++)
                {
                    Console.Write($"{myCollention[i]} ");
                }
                Console.WriteLine();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            
            

        }
    }
}
